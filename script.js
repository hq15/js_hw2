/*
Циклы нужны для решения однотинпных задач, то есть в случае когда участок кода необходимо выполнить определенное количество раз. Количество выполнения этого участка кода (действия) зависит от выполнения логического условия, если результат указанного условия будет истина то цикл (очередное выполнение действия) будет продолжено, в противном случае он или не будет начат или будет остановлен.
*/

let number;
do {
  number = +prompt("Enter your number");
} while (!Number.isInteger(number));

if (number <= 4) {
  console.log("Sorry, no numbers");
}

for (let i = 5; i <= number; i++) {
  if (i % 5 === 0) {
    console.log(i);
  }
}
alert("main completed");
console.log("main completed");

let m;
let n;

do {
  m = +prompt("Enter the first number");
  n = +prompt("Enter enter the second number greater than the first");
} while (m > n || Number.isNaN(m) || Number.isNaN(n));

for (; m <= n; m++) {
  for (let i = 2; i <= m; i++) {
    if (m % i === 0) {
      break;
    }
    let result = m;
    if (i == m - 1) {
      console.log(result);
    }
  }
}
